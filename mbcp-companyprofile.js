const bearer = "Bearer " + EightBase.store.get("auth");
const base = 'https://x2mu-0plk-3el5.n7.xano.io/api:kpFKWWat/';
var myParam = EightBase.store.get("currentID");

var request = new XMLHttpRequest();
let xanoUrl = new URL(base + 'data/get_tracked_company/' + myParam);
request.open('GET', xanoUrl.toString(), true)
request.setRequestHeader("Authorization", bearer);

request.onload = function() {
	var data = JSON.parse(this.response)
	//console.log(data);
	if (request.status >= 200 && request.status < 400) 
    {
		const t1 = document.getElementById('company-name');
		t1.textContent = data.company_name;
		const t2 = document.getElementById('domain');
		t2.textContent = data.domain;
		const t3 = document.getElementById('news-count');
		t3.textContent = data.news;
		const t4 = document.getElementById('jobs-count');
		t4.textContent = data.jobs;
		const t5 = document.getElementById('connection-count');
		t5.textContent = data.connections;
		const t6 = document.getElementById('tech-count');
		t6.textContent = data.tech;
	} 
    else 
    {
		console.log('error')
	}
}
request.send();

var newsR = new XMLHttpRequest();
let xanoNewsUrl = new URL(base + 'data/news/get_news/' + myParam);
newsR.open('GET', xanoNewsUrl.toString(), true);
newsR.setRequestHeader("Authorization", bearer);

newsR.onload = function() {
	var newsdata = JSON.parse(this.response)
	if (newsR.status >= 200 && newsR.status < 400) {
		const newsContainer = document.getElementById("mb-news-tab");
		newsdata.forEach(newsitem => {
			const newsstyle = document.getElementById('mb-panel-news');
			const newscard = newsstyle.cloneNode(true);
			newscard.setAttribute('id', '');
			const n1 = newscard.querySelector("#news-item-title");
			const link = newscard.querySelectorAll("#news-article-link")[0];
			link.setAttribute('href', newsitem.url);
			link.textContent = newsitem.title;
            newscard.querySelector("#news-item-sentence").textContent = newsitem.additional_data.article_sentence;
			var footer = newsitem.found_at + " • " + newsitem.categories.join(',') + " • " + newsitem.additional_data.article_source;
			newscard.querySelector("#news-item-footer").textContent = footer;
			newsContainer.appendChild(newscard);
		})
        newsContainer.querySelector('#mb-panel-news').outerHTML = "";

	} else {
		console.log('error')
	}
}
newsR.send();

var newsJ = new XMLHttpRequest();
let xanoJobsUrl = new URL(base + 'jobs/get_jobs/' + myParam);
newsJ.open('GET', xanoJobsUrl.toString(), true);
newsJ.setRequestHeader("Authorization", bearer);

newsJ.onload = function() {
	var jobsdata = JSON.parse(this.response); console.log(jobsdata);
	if (newsJ.status >= 200 && newsJ.status < 400) {

		const jobsContainer = document.getElementById("mb-jobs-tab");
		jobsdata.forEach(jobsitem => {

			const jobsstyle = document.getElementById('mb-panel-jobs');
			const jobscard = jobsstyle.cloneNode(true);
			jobscard.setAttribute('id', '');

			const j1 = jobscard.querySelector("#jobs-item-title");
			const link = jobscard.querySelectorAll("#jobs-item-link")[0];
			link.setAttribute('href', jobsitem.url);
			link.textContent = jobsitem.title;

			jobscard.querySelector("#jobs-item-salary").textContent +=  jobsitem.salary;
            jobscard.querySelector("#jobs-item-contract").textContent += jobsitem.contract_types.join(',');
            jobscard.querySelector("#jobs-item-location").textContent += " " + jobsitem.location;

			var footer = jobsitem.first_seen_at + " • " + jobsitem.categories.join(',');
	        jobscard.querySelector("#jobs-footer").textContent = footer;

            const jtag = jobscard.querySelector("#jobs-header");

            if(jobsitem.job_tags && jobsitem.job_tags.length);
            { 
                jobsitem.job_tags.forEach( tag =>
                {                    
                    const jobtag = jobscard.querySelector("#job-item-tag");
                    const jjtag = jobtag.cloneNode(true);
                    jjtag.setAttribute('id','');

                    jjtag.textContent = tag;
                    jtag.appendChild(jjtag);
                })
                jtag.querySelector("#job-item-tag").outerHTML = "";
            }
			jobsContainer.appendChild(jobscard);
		})
        jobsContainer.querySelector('#mb-panel-jobs').outerHTML = "";


	} else {
		console.log('error')
	}
}
newsJ.send();

var newsC = new XMLHttpRequest();
let xanoConnUrl = new URL(base + 'connections/get_connections/' + myParam);
newsC.open('GET', xanoConnUrl.toString(), true);
newsC.setRequestHeader("Authorization", bearer);

newsC.onload = function() 
{
	var conndata = JSON.parse(this.response)
	console.log(conndata);
	if (newsC.status >= 200 && newsC.status < 400) 
    {
        const connContainer = document.getElementById("conn-tab");
        conndata.categs.forEach(cid => 
            {
                const connstyle = document.getElementById('connections-category');
                const conncard = connstyle.cloneNode(true);
                conncard.setAttribute('id', '');
    
                conncard.querySelector("#conn-category-name").textContent = cid;
                connContainer.appendChild(conncard);

                const connGrid = conncard.querySelector("#conn-grid");
                conndata.connections[cid].forEach(citem => 
                    {
                        if(citem.company_name)
                        {
                        const cistyle = conncard.querySelector('#conn-item');
                        const cicard = cistyle.cloneNode(true);
                        cicard.setAttribute('id', '');
            
                        cicard.querySelector("#conn-item-name").textContent = citem.company_name;
                        cicard.querySelector("#conn-item-caption").textContent = citem.source_category;                               
                        connGrid.appendChild(cicard);
                        }
                    })
                    conncard.querySelector('#conn-item').outerHTML = "";
                //var h = conncard.querySelector('#conn-item');
                //h.className += " hide"; 

            })
        connContainer.querySelector('#connections-category').outerHTML = "";
        //var d = document.getElementById("connections-category");
        //d.className += " hide";
    }
    else {
		console.log('error')
	}
}
newsC.send();


var newsT = new XMLHttpRequest();
let xanoTechUrl = new URL(base + 'tech/get_tech/' + myParam);
newsT.open('GET', xanoTechUrl.toString(), true);
newsT.setRequestHeader("Authorization", bearer);

newsT.onload = function() {
	var techdata = JSON.parse(this.response)
	if (newsT.status >= 200 && newsR.status < 400) 
    {
		const techContainer = document.getElementById("tech-grid");
		techdata.forEach(techitem => {

			const techstyle = techContainer.querySelector('#tech-item');
			const techcard = techstyle.cloneNode(true);
			techcard.setAttribute('id', '');

			techcard.querySelector("#tech-name").textContent = techitem.title;
			techContainer.appendChild(techcard);
		})
        techContainer.querySelector('#tech-item').outerHTML = "";
	} else {
		console.log('error')
	}
}
newsT.send();

